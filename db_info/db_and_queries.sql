DROP TABLE IF EXISTS user_approves;
DROP TABLE IF EXISTS is_friends;
DROP TABLE IF EXISTS friend_requests;
DROP TABLE IF EXISTS messages;
DROP TABLE IF EXISTS saved_quotes;
DROP TABLE IF EXISTS work_tags;
DROP TABLE IF EXISTS author_tags;
DROP TABLE IF EXISTS quote_tags;
DROP TABLE IF EXISTS quotes;
DROP TABLE IF EXISTS works;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS tags;

CREATE TABLE users(
	uid SERIAL,
	name text NOT NULL,
	username text NOT NULL UNIQUE,
	password text NOT NULL,
	PRIMARY KEY(uid)
);

INSERT INTO users(name,username,password) VALUES
('Kellie','kellie','password'),
('Craig','craig','password'),
('Kat','kat','password'),
('Chris','chris','password'),
('Max','max','password'),
('David','david','password'),
('June','june','password'),
('Lauren','lauren','password'),
('Alice','alice','password'),
('Hope','hope','password');

CREATE TABLE authors(
	aid SERIAL,
	name text NOT NULL,
	bio text,
	PRIMARY KEY(aid)
);

INSERT INTO authors(name, bio) VALUES
('Neil Gaiman','Neil Gaiman Bio'),
('Simone de Beauvoir','Simone de Beauvoir Bio'),
('Kurt Vonnegut','Kurt Vonnegut Bio'),
('Rachael Carson','Rachael Carson Bio'),
('Neal Stephenson', 'Neal Stephenson Bio'),
('David Foster Wallace','David Foster Wallace Bio'),
('Victoria Schwab','Victoria Schwab Bio'),
('Vladmir Nabokov','Vladmir Nabokov Bio'),
('Aldous Huxley','Aldous Huxley Bio'),
('Douglas Adams','Douglas Adams Bio');

CREATE TABLE tags(
	name text,
	PRIMARY KEY(name)
);

INSERT INTO tags(name) VALUES
('beliefs'),
('feminism'),
('ideas'),
('lies'),
('nature'),
('information'),
('code'),
('words'),
('moments'),
('lives'),
('imagination'),
('comfort'),
('dream'),
('fantasy'),
('science fiction'),
('environment'),
('humor'),
('dystopia'),
('utopia'),
('literature'),
('memoir'),
('nonfiction'),
('fiction');

CREATE TABLE works(
	wid SERIAL,
	name text NOT NULL,
	aid int NOT NULL,
	PRIMARY KEY(wid), 
	FOREIGN KEY(aid) REFERENCES authors(aid)
);

INSERT INTO works(name, aid) VALUES
('American Gods',1),
('Second Sex',2),
('Slaugterhouse-Five',3),
('Silent Spring',4),
('Snow Crash',5),
('The Pale King',6),
('Vicious',7),
('Lolita',8),
('Brave New World',9),
('The Hitchhiker''s Guide to the Galaxy',10);


CREATE TABLE quotes(
	qid SERIAL,
	content text NOT NULL UNIQUE,
	submitted_by int NOT NULL,
	num_approvals int NOT NULL,
	work int,
	author int NOT NULL,
	added_time timestamp,
	PRIMARY KEY(qid),
	FOREIGN KEY(submitted_by) REFERENCES users(uid),
	FOREIGN KEY(work) REFERENCES works(wid),
	FOREIGN KEY(author) REFERENCES authors(aid)
);

INSERT INTO quotes(content,submitted_by,num_approvals,work,author,added_time) VALUES 
('People believe, thought Shadow. It''s what people do. They believe, and then they do not take responsibility for their beliefs; they conjure things, and do not trust the conjuration. People populate the darkness; with ghosts, with gods, with electrons, with tales. People imagine, and people believe; and it is that rock solid belief, that makes things happen.',1,0,1,1,'2016-10-19 10:23:53+02'),
('One is not born, but rather becomes, a woman.',1,12,2,2,'2016-10-19 10:23:53+02'),
('I think you guys are going to have to come up with a lot of wonderful new lies, or people just aren''t going to want to go on living.',3,6,3,3,'2016-10-19 10:23:53+02'),
('Those who contemplate the beauty of the earth find reserves of strength that will endure as long as life lasts. There is something infinitely healing in the repeated refrains of nature -- the assurance that dawn comes after night, and spring after winter.',4,11,4,4,'2016-10-19 10:23:53+02'),
('Well, all information looks like noise until you break the code.',5,16,5,5,'2016-10-19 10:23:53+02'),
('How odd I can have all this inside me and to you it''s just words.',6,2,6,6,'2016-10-19 10:23:53+02'),
('The moments that define lives aren''t always obvious. They don''t scream LEDGE, and nine times out of ten there''s no rope to duck under, no line to cross, no blood pact, no official letter on fancy paper. They aren''t always protracted, heavy with meaning. Between one sip and the next, Victor made the biggest mistake of his life, and it was made of nothing more than one line. Three small words.',7,4,7,7,'2016-10-19 10:23:53+02'),
('I need you, the reader, to imagine us, for we don''t really exist if you don''t.',8,4,8,8,'2016-10-19 10:23:53+02'),
('But I don''t want comfort. I want God, I want poetry, I want real danger, I want freedom, I want goodness. I want sin.',9,16,9,9,'2016-10-19 10:23:53+02'),
('He felt that his whole life was some kind of dream and he sometimes wondered whose it was and whether they were enjoying it.',10,19,10,10,'2016-10-19 10:23:53+02');


CREATE TABLE quote_tags(
	qid int,
	tag text,
	PRIMARY KEY(qid,tag),
	FOREIGN KEY(qid) REFERENCES quotes(qid) ON DELETE CASCADE,
	FOREIGN KEY(tag) REFERENCES tags(name) ON DELETE CASCADE
);

INSERT INTO quote_tags(qid,tag) VALUES
(1,'beliefs'),
(2,'feminism'),
(3,'ideas'),
(3,'lies'),
(3,'humor'),
(4,'nature'),
(5,'information'),
(5,'code'),
(6,'words'),
(7,'moments'),
(7,'lives'),
(8,'imagination'),
(8,'beliefs'),
(9,'comfort'),
(10,'dream');

CREATE TABLE author_tags(
	aid int,
	tag text,
	PRIMARY KEY(aid,tag),
	FOREIGN KEY(aid) REFERENCES authors(aid) ON DELETE CASCADE,
	FOREIGN KEY(tag) REFERENCES tags(name) ON DELETE CASCADE
);

INSERT INTO author_tags(aid,tag) VALUES
(1,'fantasy'),
(1,'fiction'),
(2,'feminism'),
(2,'fiction'),
(3,'environment'),
(3,'science fiction'),
(6,'fiction'),
(7,'fiction'),
(7,'fantasy'),
(8,'fiction'),
(9,'science fiction'),
(9,'dystopia'),
(10,'science fiction');

CREATE TABLE work_tags(
	wid int,
	tag text,
	PRIMARY KEY(wid,tag),
	FOREIGN KEY(wid) REFERENCES works(wid) ON DELETE CASCADE,
	FOREIGN KEY(tag) REFERENCES tags(name) ON DELETE CASCADE
);

INSERT INTO work_tags(wid,tag) VALUES
(1,'fantasy'),
(1,'fiction'),
(2,'feminism'),
(3,'fiction'),
(4,'environment'),
(5,'science fiction'),
(6,'fiction'),
(7,'fiction'),
(7,'fantasy'),
(8,'fiction'),
(9,'science fiction'),
(9,'dystopia'),
(10,'science fiction');

CREATE TABLE saved_quotes(
	uid int, 
	qid int,
	PRIMARY KEY(uid,qid),
	FOREIGN KEY(uid) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(qid) REFERENCES quotes(qid) ON DELETE CASCADE
);

INSERT INTO saved_quotes(uid,qid) VALUES
(1,1),
(1,7),
(2,8),
(3,9),
(4,10),
(5,5),
(6,3),
(7,7),
(8,4),
(9,6),
(10,3);

CREATE TABLE messages(
	sender int,
	receiver int,
	qid int,
	mid SERIAL ,
	content text,
	mtimestamp timestamp NOT NULL,
	PRIMARY KEY(sender,receiver, mid),
	FOREIGN KEY(sender) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(receiver) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(qid) REFERENCES quotes(qid)
);

INSERT INTO messages(sender,receiver,qid,content,mtimestamp) VALUES
(10,1,1,'Triggered','2016-10-19 10:23:53+02'),
(4,8,5,'C''est La Vie','2016-10-19 10:26:54+02'),
(2,3,4,'Lolz','2016-10-19 10:24:55+02'),
(1,2,7,'-_-','2016-10-19 10:15:55+02'),
(5,2,9,'^-^','2016-10-19 10:24:55+02'),
(2,5,4,'D:','2016-10-19 10:24:55+02'),
(3,4,5,'So broken','2016-10-19 10:24:55+02'),
(1,2,NULL,'Haha','2016-10-19 10:24:55+02'),
(8,9,3,'Well...','2016-10-19 10:24:55+02'),
(5,3,1,'Cancer','2016-10-19 10:24:55+02'),
(1,2,NULL,'Hehe','2016-10-19 10:24:55+02'),
(1,2,NULL,'Hoho','2016-10-19 10:24:55+02');

CREATE TABLE friend_requests(
	sender int,
	receiver int,
	PRIMARY KEY(sender,receiver),
	FOREIGN KEY(sender) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(receiver) REFERENCES users(uid) ON DELETE CASCADE,
	CHECK ( sender != receiver )
);

CREATE TABLE is_friends(
	uid1 int,
	uid2 int,
	PRIMARY KEY(uid1,uid2),
	FOREIGN KEY(uid1) REFERENCES users(uid),
	FOREIGN KEY(uid2) REFERENCES users(uid)
);

INSERT INTO is_friends(uid1,uid2) VALUES
(8,9),
(3,4),
(1,2),
(3,5),
(1,10),
(4,8),
(2,3),
(2,5),
(4,6),
(2,6),
(3,7),
(3,9),
(7,9);

CREATE TABLE user_approves(
	uid int,
	qid int,
	PRIMARY KEY(uid,qid),
	FOREIGN KEY(uid) REFERENCES users(uid),
	FOREIGN KEY(uid) REFERENCES users(uid)
);
/* Queries */

/* Find quotes that have been saved by more than one user */
SELECT quotes.content, count(*) 
FROM quotes,saved_quotes 
WHERE saved_quotes.qid = quotes.qid 
GROUP BY quotes.content
HAVING count(*) > 1;

/* Find mutual friends of two users with uid 3 and uid 8*/
((SELECT uid2 FROM is_friends WHERE uid1 = 3) UNION  (SELECT uid1 FROM is_friends WHERE uid2 = 3))
INTERSECT
((SELECT uid2 FROM is_friends WHERE uid1 = 8) UNION  (SELECT uid1 FROM is_friends WHERE uid2 = 8));

/* Return all quotes, authors, and works that share the same tag */
SELECT label FROM 
((SELECT quotes.qid as id,quotes.content as label
FROM quote_tags, quotes
WHERE quote_tags.qid = quotes.qid and quote_tags.tag = 'feminism')
UNION
(SELECT authors.aid as id, authors.name as label
FROM author_tags, authors
WHERE author_tags.aid = authors.aid and author_tags.tag = 'feminism')
UNION
(SELECT works.wid as id, works.name as label
FROM work_tags,works
WHERE work_tags.wid = works.wid and work_tags.tag = 'feminism')) as labels;/


/* Given an author, pull up quotes and works */
SELECT quotes.content AS quote,works.name AS work
FROM authors,quotes,works
WHERE authors.name ='Neil Gaiman' and authors.aid = quotes.author and authors.aid = works.aid;


/* Given quote, pull up author and works */
SELECT authors.name AS author, works.name AS work
FROM quotes,authors,works
WHERE quotes.qid = 1000 and quotes.work = works.wid and quotes.author = authors.aid;


/* Given a work, pull up author and quotes */
SELECT authors.name AS author,quotes.content AS quote
FROM works,authors,quotes
WHERE works.name='American Gods' and works.aid = authors.aid and quotes.work = works.wid;


/* Find the top 10 most saved quotes */
SELECT quotes.content AS quote, count(*) AS times_saved
FROM saved_quotes, quotes
WHERE saved_quotes.qid = quotes.qid
GROUP BY quotes.content
ORDER BY count(*) DESC
LIMIT 10;

/* Find the top 10 newest quotes */
SELECT quotes.content as quote
FROM quotes
ORDER BY quotes.added_time ASC
LIMIT 10;

/* UDF for semantic similarity */

/* See saved quotes for a user*/
SELECT quotes.content
FROM saved_quotes,quotes
WHERE saved_quotes.uid = 1 and saved_quotes.qid = quotes.qid

/* See saved quotes between two users*/
(SELECT quotes.content
FROM saved_quotes,quotes
WHERE saved_quotes.uid = 1 and saved_quotes.qid = quotes.qid)
INTERSECT
(SELECT quotes.content
FROM saved_quotes,quotes
WHERE saved_quotes.uid = 2 and saved_quotes.qid = quotes.qid)

/* Trigger for sending friend request*/
CREATE TRIGGER send_friend_request
BEFORE INSERT ON friend_requests
FOR EACH ROW
	BEGIN %% sql
		IF NEW IN is_friends
			RETURN NULL
		END IF;
		RETURN NEW
	END;

/* Sending a friend request if not friended already */
INSERT INTO friend_requests VALUES(1,2)

/* Friend request rejection */
 DELETE FROM friend_requests WHERE friend_requests.uid1 = 1 and friend_requests.uid2 = 2

/* Friend request acceptance */
 INSERT INTO is_friends VALUES(1,2)
 DELETE FROM friend_requests WHERE friend_requests.uid1 = 1 and friend_requests.uid2 = 2

/* See friend's list */
SELECT name 
FROM users
WHERE users.uid IN ((SELECT uid2 FROM is_friends WHERE uid1 = 3) UNION  (SELECT uid1 FROM is_friends WHERE uid2 = 3))

/* See messages between two users*/
SELECT users.name AS sender, users2.name AS receiver, messages.content AS message,quotes.content AS quote, messages.mtimestamp AS mtimestamp
FROM messages,quotes,users, users AS users2
WHERE 
((messages.sender = 1 and messages.receiver = 2) or (messages.receiver = 1 and messages.sender = 2))
and messages.qid = quotes.qid
and messages.sender = users.uid 
and messages.receiver = users2.uid
ORDER BY mtimestamp DESC

SELECT users.name AS sender, users2.name AS receiver, messages.content AS message,quotes.content AS quote, messages.mtimestamp AS mtimestamp FROM messages,quotes,users, users AS users2 WHERE ((messages.sender = 1 and messages.receiver = 2) or (messages.receiver = 1 and messages.sender = 2)) and messages.qid = quotes.qid and messages.sender = users.uid and messages.receiver = users2.uid ORDER BY mtimestamp DESC

/* See all users you've sent messages to */
SELECT users.name, users.uid
FROM messages m, users u1, users u2
WHERE m.sender = 1 or m.receiver = 1
and m.sender = u1.uid and m.receiver = u2.uid

/* See all received messages */
SELECT u.name AS sender, m.content AS message,q.content AS quote, m.mtimestamp AS mtimestamp
FROM (messages m, users u) LEFT JOIN quotes q ON m.qid = q.qid
WHERE m.receiver = 1 and m.sender = u.uid

/* See all sent messages */
SELECT u.name AS receiver, m.content AS message,q.content AS quote, m.mtimestamp AS mtimestamp
FROM (messages m, users u) LEFT JOIN quotes q ON m.qid = q.qid
WHERE m.sender = 1 and m.receiver = u.uid

/* Insert a quote */
INSERT INTO quotes VALUES(1000,'quote here',13,9,100,10,now())

/* Vote on a quote */
SELECT num_approvals
FROM quotes
WHERE quotes.qid = 1000 

UPDATE quotes
SET num_approvals = 13
WHERE qid = 1000

/* Retrieve unapproved quotes */
SELECT *
FROM quotes
WHERE num_approvals < 10
 
/* Find all users you've (0 ) have sent or received a message from */
(SELECT users.username AS username, users.uid AS uid
FROM messages,quotes,users, users AS users2
WHERE 
messages.sender = 1
and messages.qid = quotes.qid
and messages.sender = users.uid 
and messages.receiver = users2.uid
ORDER BY mtimestamp DESC)
UNION
(SELECT users.username AS username, users.uid AS uid
FROM messages,quotes,users, users AS users2
WHERE 
messages.receiver = 1
and messages.qid = quotes.qid
and messages.sender = users.uid 
and messages.receiver = users2.uid
ORDER BY mtimestamp DESC)

