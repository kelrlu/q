''' Python code for UDF for semantic similarity between sentences '''
from nltk.corpus import wordnet as wn
import numpy as np
import re


def sentence_semantic_vector(s,joint_word_set):
	s_words = re.sub("[^\w-]"," ",s1).split()
	sv = [0] * len(joint_word_set)
	# Fill sv vector
	for i in range(0,len(joint_word_set))
		sv[i] = 1 if joint_word_set[i] in s_words else word_semantic_similarity(joint_word_set[i],s)[1]
	return sv


def sentence_word_order_vector(s,joint_word_set):
	s_words = re.sub("[^\w-]"," ",s1).split()
	wov = [0] * len(joint_word_set)
	# Fill wov vector
	for i in range(0,len(joint_word_set))
		try:
			index = s_words.index(joint_word_set[i])
			wov[i] = index
		except ValueError:
			index = word_semantic_similarity(joint_word_set[i],s)[2]
			wov[i] = index

def word_path_length(w1,w2):
	w1_synset = wn.synset(w1)
	w1_synset_words = 
	w2_synset = 
	w2_synset_words = 



def word_depth(w1,w2):


def word_semantic_similarity(w,s):
	s_words = re.sub("[^\w-]"," ",s1).split()
	word = ""
	similiarity_score = -1
	index = -1

	for i in range(0,len(s_words)):
		path_length = word_path_length(w, s_words[i])
		depth = word_depth(w,s_words[i])
		if(path_length * depth > similiarity_score):
			word = s_words[i]
			similiarity_score = path_length * depth
			index = s_words.index(s_words[i])

	return [word, similiarity_score, index]



def joint_word_set(s1,s2):
	s1_words = re.sub("[^\w-]"," ",s1).split()
	s2_words = re.sub("[^\w-]"," ",s2).split()
	return list(set(s1_words) | set(s2_words))

''' Main function for finding sentence similarity between two sentences ''' 
def ss(s1,s2,sigma):
	# Get joint_word set, semantic vectors, word order vectors
	joint_word_set = joint_word_set(s1,s2)
	sv1 = sentence_semantic_vector(s1,joint_word_set)
	sv2 = sentence_semantic_vector(s2,joint_word_set)
	wov1 = sentence_word_order_vector(s1,joint_word_set)
	wov2 = sentence_word_order_vector(s2,joint_word_set)
	# Perform calculation
	sv = sigma * ( np.dot(sv1,sv2) / (np.norm(sv1) * np.norm(sv2)) )
	wov = (1 - sigma) * ( np.norm(np.subtract(wov1,wov2)) / np.norm(np.add(wov1,wov2)) )
	return sv + wov



