%%sql

DROP TABLE IF EXISTS is_friends;
DROP TABLE IF EXISTS friend_requests;
DROP TABLE IF EXISTS messages;
DROP TABLE IF EXISTS saved_quotes;
DROP TABLE IF EXISTS work_tags;
DROP TABLE IF EXISTS author_tags;
DROP TABLE IF EXISTS quote_tags;
DROP TABLE IF EXISTS quotes;
DROP TABLE IF EXISTS works;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS tags;

CREATE TABLE users(
	uid int,
	name text NOT NULL,
	username text NOT NULL UNIQUE,
	password text NOT NULL,
	PRIMARY KEY(uid)
);

CREATE TABLE authors(
	aid int,
	name text NOT NULL,
	bio text,
	PRIMARY KEY(aid)
);

CREATE TABLE tags(
	name text,
	PRIMARY KEY(name)
);

CREATE TABLE works(
	wid int,
	name text NOT NULL,
	aid int NOT NULL,
	PRIMARY KEY(wid), 
	FOREIGN KEY(aid) REFERENCES authors(aid)
);

CREATE TABLE quotes(
	qid int,
	content text NOT NULL UNIQUE,
	submitted_by int NOT NULL,
	num_approvals int NOT NULL,
	work int,
	author int NOT NULL,
	added_time timestamp,
	PRIMARY KEY(qid),
	FOREIGN KEY(submitted_by) REFERENCES users(uid),
	FOREIGN KEY(work) REFERENCES works(wid),
	FOREIGN KEY(author) REFERENCES authors(aid)
);

CREATE TABLE quote_tags(
	qid int,
	tag text,
	PRIMARY KEY(qid,tag),
	FOREIGN KEY(qid) REFERENCES quotes(qid) ON DELETE CASCADE,
	FOREIGN KEY(tag) REFERENCES tags(name) ON DELETE CASCADE
);

CREATE TABLE author_tags(
	aid int,
	tag text,
	PRIMARY KEY(aid,tag),
	FOREIGN KEY(aid) REFERENCES authors(aid) ON DELETE CASCADE,
	FOREIGN KEY(tag) REFERENCES tags(name) ON DELETE CASCADE
);

CREATE TABLE work_tags(
	wid int,
	tag text,
	PRIMARY KEY(wid,tag),
	FOREIGN KEY(wid) REFERENCES works(wid) ON DELETE CASCADE,
	FOREIGN KEY(tag) REFERENCES tags(name) ON DELETE CASCADE
);

CREATE TABLE saved_quotes(
	uid int, 
	qid int,
	PRIMARY KEY(uid,qid),
	FOREIGN KEY(uid) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(qid) REFERENCES quotes(qid) ON DELETE CASCADE
);

CREATE TABLE messages(
	sender int,
	receiver int,
	qid int,
	mid int,
	content text,
	mtimestamp timestamp NOT NULL,
	PRIMARY KEY(sender,receiver, mid),
	FOREIGN KEY(sender) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(receiver) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(qid) REFERENCES quotes(qid)
	);

CREATE TABLE friend_requests(
	sender int,
	receiver int,
	PRIMARY KEY(sender,receiver),
	FOREIGN KEY(sender) REFERENCES users(uid) ON DELETE CASCADE,
	FOREIGN KEY(receiver) REFERENCES users(uid) ON DELETE CASCADE,
	CHECK ( sender != receiver )
);

CREATE TABLE is_friends(
	uid1 int,
	uid2 int,
	PRIMARY KEY(uid1,uid2),
	FOREIGN KEY(uid1) REFERENCES users(uid),
	FOREIGN KEY(uid2) REFERENCES users(uid),
	CHECK (uid1 < uid2)
);



