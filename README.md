## Q
Q is a Flask-based quote collecting application similar to Goodreads. Q allows users to save their favorite quotes and recommends quotes to users based upon their saved quotes using the semantic similarity algorithm found in *Sentence Similarity Based on Semantic Nets and Corpus Statistics* by Y. Li, D. McLean, Z. Bandar, J.D. O'Shea, and K. Crockett.

Q was started using the skeleton tutorial Flask Code.

## Running
1. To deploy the application, open terminal/console and change to the project directory.

2. Then, run the following commands in terminal in the project directory.

   ```
   python server.py
   ```

3. Visit http://localhost:8111 in your browser to interact with the application. 


